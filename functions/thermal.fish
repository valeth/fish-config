function thermal
    function _thermal
        for z in (find /sys/class/thermal/thermal_zone*)
            echo (echo $z | sed 's|.*\(thermal_zone[0-9]\)|\1|') (math (cat $z/temp)'/ 1000')
        end
    end

    set -l options $argv "default"
    switch $options[1]
        case "watch"
            while true
                clear
                _thermal
                sleep 2
            end
        case "default"
            _thermal
    end
end
