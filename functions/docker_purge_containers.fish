function docker_purge_containers
    echo 'This will remove ALL docker containers.'
    read -p 'echo "Are you sure? [yN] "' -l choice

    if test $choice = 'y'
        set -l containers (docker ps -aq)
        if test (count $containers) -eq 0
            echo 'No containers found'
        else
            command docker rm --force $containers
        end
    end
end
