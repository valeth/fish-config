function gitchanges
    set -l workspace_dir $HOME/Workspace

    for dir in (find $workspace_dir -regex '.*[^/]\.git' -type d)
        pushd $dir

        if test -d .git
            set -l git_count (git status --porcelain --untracked-files=no | wc -l)

            if test $git_count -gt 0
                printf '%3s %s\n' $git_count "modified files in $dir"
            end
        end

        popd $dir
    end
end
