function updates
    if set -q argv[1]; and test $argv[1] = '-c'
        echo (command pacman -Qqu | wc -l) Packages
    else
        command pacman -Qu
    end
end
