function xdg_wrapper --description 'Wrap a command so that it does not dump files into $HOME.'
    set -l argc (count $argv)

    test $argc -lt 3; and return 1
    set -l type $argv[1]
    set -l categories $argv[2]
    set -l cmd $argv[3]

    test $argc -ge 4; and set -l args $argv[4..-1]

    # only create the wrapper if the binary exists
    type -fq $cmd; or return 2

    set -l opt_dir (xdg_home opt/$cmd)

    test -d $opt_dir; or command mkdir -p $opt_dir

    echo -e "function $cmd\ncommand env HOME=$opt_dir $cmd $args \$argv\nend" | source -

    if test $type = '-d'
        set -l desktop_file (xdg_home data applications/$cmd.desktop)
        set -l cmd_name (echo $cmd | sed 's|[^ _-]*|\u&|g')

        if test ! -e $desktop_file
            echo -e "[Desktop Entry]\nName=$cmd_name\nExec=/usr/bin/env HOME=$opt_dir $cmd $args %U\nIcon=$cmd\nType=Application\nCategories=$categories;\n" > $desktop_file
        end
    end
end
