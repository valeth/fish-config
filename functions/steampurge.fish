function steampurge
    set -l steam_root $XDG_DATA_HOME/Steam
    set -l steam_root2 $XDG_HOME/opt/steam/.steam/root

    find $steam_root2 \( \
        -name "libgcc_s.so*" \
        -o -name "libstdc++.so*" \
        -o -name "libxcb.so*" \
        -o -name "libgpg-error.so*" \) \
        -print -delete

    find $steam_root \( \
        -name "libgcc_s.so*" \
        -o -name "libstdc++.so*" \
        -o -name "libxcb.so*" \
        -o -name "libgpg-error.so*" \) \
        -print -delete
end
