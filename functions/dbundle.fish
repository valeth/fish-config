function dbundle
    set -x BUNDLE_TRAMPOLINE_DISABLE 1
    set repo_path $HOME/Workspace/Tools/bundler.git
    ruby -I $repo_path/lib $repo_path/exe/bundler $argv
end
