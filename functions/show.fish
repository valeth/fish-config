function show -d 'show file or directory content'
    if test -z $argv[1]
        set argv $PWD
    end

    for f in $argv
        if test -d $f
            ls -l $f
        else if test (file --dereference --brief --mime-encoding $f) != binary
            echo $f
            command cat $f
            echo
        else
            echo $f is a binary file
        end
    end
end
