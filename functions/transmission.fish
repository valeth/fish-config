function transmission
    if systemctl --user is-active transmission >/dev/null
        systemctl --user stop transmission
    else
        systemctl --user start transmission
    end
end
