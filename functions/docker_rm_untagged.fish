function docker_rm_untagged
    echo 'This will remove ALL untagged docker container images.'
    read -p 'echo "Are you sure? [yN] "' -l choice

    if test $choice = 'y'
        set -l images (docker images | grep "^<none>" | awk '{print $3}')
        if test (count $images) -eq 0
            echo 'No untagged container images found'
        else
            command docker rmi --force $images
        end
    end
end
