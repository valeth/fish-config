function colstr
    set -l color $argv[1]
    set -l str $argv[2]
    set -l colors 'red' 'green' 'yellow' 'blue' 'magenta' 'cyan' 'white' 'lightgrey'
    set -l index (contains -i $color $colors)

    if test -n $index
        echo -ne "\e[38;5;"$index"m"
    else
        echo -ne "\e[38;5;"$color"m"
    end

    echo -ne "$str\e[m"
end