function ls --description "List contents of directory"
    set -l param "--color=auto"
    set param $param "--group-directories-first"
    set param $param "--sort=extension"

    if isatty 1
        set param $param "--indicator-style=classify"
    end

    command ls $param $argv
end
