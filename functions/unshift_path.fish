function unshift_path
    set -l var $argv[1]

    for arg in $argv[-1..2]
        if test -d $arg; and not eval contains $arg \$$var
            eval "set -x $argv[1] $arg \$$var"
        end
    end
end
