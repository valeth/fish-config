function clear-kde-cache
    function cleanup_cache_kde4
        if test -d /var/tmp/kdecache-$USER
            echo "Clearing KDE4 cache..."
            command rm -rf /var/tmp/kdecache-$USER ^/dev/null
        end
    end

    function cleanup_cache_kf5
        echo "Clearing KF5 cache..."
        for f in $XDG_CACHE_HOME/{plasma*,ksycoca5*,*.kcache}
            command rm $f ^/dev/null
        end
    end

    function rebuild_cache_kde4
        if test -x /usr/bin/kbuildsyscoca4
            echo "Running kbuildsycoca4..."
            kbuildsycoca4 --noincremental ^/dev/null
        end
    end

    function rebuild_cache_kf5
        if test -x /usr/bin/kbuildsycoca5
            echo "Running kbuildsycoca5..."
            kbuildsycoca5 --noincremental ^/dev/null
        end
    end

    set -l option $argv[1] "default"
    switch $option[1]
        case "kde4"
            cleanup_cache_kde4
            rebuild_cache_kde4
        case "kf5"
            cleanup_cache_kf5
            rebuild_cache_kf5
        case "*"
            cleanup_cache_kde4
            cleanup_cache_kf5
            rebuild_cache_kde4
            rebuild_cache_kf5
    end
end
