function onchange
    set -l directory $argv[1]
    set -l cmd $argv[2..-1]

    set -l c_cmd "lightgrey"

    if not set -q cmd
        echo 'No command specified'
        exit 1
    end

    echo -e "watching "(colstr $c_cmd $directory)
    echo -e "executing "(colstr $c_cmd $cmd)" on change\n"

    while true
        inotifywait --quiet \
                    --recursive \
                    --event modify \
                    --event create \
                    $directory > /dev/null

        if test $status -eq 0
            sleep 2
            set -l timestamp (colstr cyan [(date '+%F %T:%N')])
            set -l exec_info "{pwd: "(colstr $c_cmd $PWD)", watch: "(colstr $c_cmd $directory)", exec: "(colstr $c_cmd "$cmd")"}"

            echo -e "$timestamp  "(colstr yellow EXECUTING)"  $exec_info"
            eval $cmd

            if test $status -eq 0
                echo -e "$timestamp  "(colstr green SUCCESS)"  $exec_info"
            else
                echo -e "$timestamp  "(colstr red ERROR)"  $exec_info"
                break
            end
        end
    end
end