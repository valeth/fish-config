function what
    set -l binpaths
    set -l ret 1

    if test (count $argv) -gt 0
        set binpaths (type -fp $argv ^ /dev/null)
        if test (count $binpaths) -gt 0
            command file $binpaths; and set ret 0
        end
    end

    return $ret
end
