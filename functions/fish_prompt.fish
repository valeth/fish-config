function fish_prompt
    function _pwd
        set -l wd (command pwd)

        if test $wd = $HOME
            echo -n '~'
        else
            set -l tmp (string split / $wd)

            if test (count $tmp) -gt 3
                set tmp $tmp[-2..-1]
            end

            echo -n (string join / $tmp)
        end
    end

    if test -n $POWERLINE_CONFIG_COMMAND
        echo -n '('
        set_color green
        echo -n $USER
        set_color normal
        echo -n '@'
        set_color blue
        echo -n (hostname)
        set_color normal
        echo -n ':'(_pwd)
        echo -n ')> '
    end
end
