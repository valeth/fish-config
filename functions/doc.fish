function doc
    switch $argv[1]
        case "-r"
            ri $argv[2..-1]
        case "-p"
            pydoc $argv[2..-1]
        case '*'
            man $argv
    end
end
