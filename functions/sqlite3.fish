function sqlite3
    set -l SQLITE3_HOME $XDG_HOME/opt/sqlite3

    test -d $SQLITE3_HOME; or mkdir -p $SQLITE3_HOME >/dev/null

    command env HOME=$SQLITE3_HOME sqlite3 $argv
end
