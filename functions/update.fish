function update
	function arch_update
        if type -qP sudo
            sudo pacman -Syu $argv
        else
            su -c "pacman -Syu $argv"
        end
    end

    if type -qP pacman
        arch_update $argv
    else
        echo 'Package manager not supported.'
        exit 1
    end
end
