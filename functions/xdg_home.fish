function xdg_home
    set -l argc (count $argv)
    set type $argv[1]

    if test $argc -eq 0
        echo (fetch_var XDG_HOME $HOME/.local)
    else
        if test $argc -gt 1
            set subdir /$argv[2]
        else
            set subdir ''
        end

        switch $type
        case config
            echo (fetch_var XDG_CONFIG_HOME $HOME/.config)$subdir
        case data
            echo (fetch_var XDG_DATA_HOME $HOME/.local/share)$subdir
        case cache
            echo (fetch_var XDG_CACHE_HOME $HOME/.cache)$subdir
        case bin
            echo (fetch_var XDG_BIN_HOME $HOME/.local/bin)$subdir
        case runtime
            echo (fetch_var XDG_RUNTIME_DIR /run/user/1000)$subdir
        case '*'
            set subdir /$type
            echo (fetch_var XDG_HOME $HOME/.local)$subdir
        end
    end
end
