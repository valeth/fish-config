function scp --wraps="scp"
    set -l ssh_config_home "$TOOLCHAIN_ROOT/core/ssh"

    if not pidof gpg-agent >/dev/null
        gpgconf --launch gpg-agent
    end

    command scp -F $ssh_config_home/config $argv
end
