status is-interactive; or exit

# xdg_wrapper -d 'Development' atom
# xdg_wrapper -n 'Development' bundle
xdg_wrapper -n 'Development' cabal
# xdg_wrapper -n 'Development' docker
xdg_wrapper -n 'Development' pylint
xdg_wrapper -n 'Development' ghc
xdg_wrapper -n 'Development' heroku
# xdg_wrapper -n 'Development' npm
# xdg_wrapper -n 'Development' root -l
xdg_wrapper -n 'Development' adb
# xdg_wrapper -d 'Development' code-oss
# xdg_wrapper -d 'Development' code-insiders

xdg_wrapper -d 'Office;Network' thunderbird
# xdg_wrapper -d 'Office;Network' kmail
# xdg_wrapper -d 'Office;Network' kontact

# xdg_wrapper -d 'Network;WebBrowser' chromium
# xdg_wrapper -d 'Network' discord
# xdg_wrapper -d 'Network' discord-canary
xdg_wrapper -d 'Network;WebBrowser' firefox
xdg_wrapper -d 'Network;WebBrowser' firefox-nightly

xdg_wrapper -d 'Game' steam -dev
xdg_wrapper -d 'Game' steam-native -dev
xdg_wrapper -d 'Game' playonlinux