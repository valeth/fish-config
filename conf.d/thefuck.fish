status --is-interactive; or exit

if type -qP thefuck
    eval (thefuck --alias | tr '\n' ';')
end
