status --is-interactive; or exit

abbr -a cp          'cp --verbose --interactive'
abbr -a dfh         'df --human-readable'
abbr -a duh         'du --human-readable --summarize --total'
abbr -a ghci        'ghc --interactive -ghci-script '(xdg_home config 'ghci/ghci.hs')
abbr -a heroku_git  'env HOME='(xdg_home 'opt/heroku')' git'
abbr -a htop        "htop --user=$USER"
abbr -a irc         'weechat-curses'
abbr -a la          'ls -lA'
abbr -a ll          'ls -l'
abbr -a ln          'ln --verbose --interactive'
abbr -a mgrep       'lsmod | grep'
abbr -a mkdir       'mkdir --verbose --parents'
abbr -a ocaml       'rlwrap ocaml'
abbr -a ping        'ping -c 3'
abbr -a powertop    'sudo powertop'
abbr -a py          'ipython'
abbr -a rb          'pry'
abbr -a reboot      'systemctl --system reboot'
abbr -a rmdir       'rmdir --verbose'
abbr -a ruby        'ruby -w'
abbr -a ssh         'ssh -F '(xdg_home 'toolchain/core/ssh/config')
abbr -a sshfs       'sshfs -F '(xdg_home 'toolchain/core/ssh/config')
abbr -a svi         'env EDITOR=nvim sudo -e'
abbr -a tmux        'tmux -2 -f '(xdg_home config 'tmux/config.tmux')
abbr -a tracepath   'tracepath -m 20 -b'
abbr -a userctl     'systemctl --user'
abbr -a vi          'nvim'
abbr -a vim         'nvim'
abbr -a watch       'watch --color'
abbr -a weather     'curl http://wttr.in'
