set -x GPG_TTY (tty)

if status is-login
    set -x XDG_CONFIG_HOME (xdg_home config)
    set -x XDG_CACHE_HOME  (xdg_home cache)
    set -x XDG_DATA_HOME   (xdg_home data)
    set -x XDG_BIN_HOME    (xdg_home bin)
    set -x XDG_HOME        (xdg_home)

    set -x EDITOR nvim
    set fish_greeting
    set -e CABAL_SANDBOX_CONFIG
    set -x VAGRANT_HOME (xdg_home data 'vagrant')
    set -x WAKATIME_HOME (xdg_home data 'wakatime')

    set -x CDPATH
    unshift_path CDPATH /run/media/$USER
    unshift_path CDPATH $HOME/Workspace
    unshift_path CDPATH $HOME
    unshift_path CDPATH '.'

    unshift_path PATH /usr/lib/ccache/bin
    unshift_path PATH (xdg_home bin)

    set -x TOOLCHAIN_ROOT (xdg_home 'toolchain')

    # Ruby
    set -x RBENV_ROOT "$TOOLCHAIN_ROOT/rbenv"
    set -x RUBY_BUILD_CACHE_PATH (xdg_home cache 'ruby-build')
    unshift_path PATH "$RBENV_ROOT/bin"

    # Python
    set -x PYTHON_EGG_CACHE (xdg_home cache 'python-eggs')
    set -x IPYTHONDIR (xdg_home config 'ipython')
    set -x PYLINTHOME (xdg_home cache 'pylint')
    set -x PYENV_ROOT "$TOOLCHAIN_ROOT/pyenv"
    set -x PYTHON_BUILD_CACHE_PATH (xdg_home cache 'python-build')
    unshift_path PATH "$PYENV_ROOT/bin"

    # Node.js
    set -x NODENV_ROOT "$TOOLCHAIN_ROOT/nodenv"
    set -x NODE_BUILD_CACHE_PATH (xdg_home cache 'node-build')
    unshift_path PATH "$NODENV_ROOT/bin"

    # Crystal
    set -x CRENV_ROOT "$TOOLCHAIN_ROOT/crenv"
    unshift_path PATH "$CRENV_ROOT/bin"

    # Rust
    set -x RUSTUP_HOME "$TOOLCHAIN_ROOT/rustup"
    set -x CARGO_HOME "$TOOLCHAIN_ROOT/cargo"
    unshift_path PATH "$CARGO_HOME/bin"

    # Haskell
    set -x STACK_ROOT "$TOOLCHAIN_ROOT/stack"
    unshift_path PATH "$STACK_ROOT/bin"
end


if status is-interactive
    # Ruby
    if test -d $RBENV_ROOT
        source (rbenv init -|psub)
    end

    # Python
    if test -d $PYENV_ROOT
        source (pyenv init -|psub)
        source (pyenv virtualenv-init -|psub)

        if type -qP powerline
            append_path fish_function_path "$PYENV_ROOT/versions/3.6.2/lib/python3.6/site-packages/powerline/bindings/fish"
            powerline-daemon -q
            powerline-setup
        end
    end

    # Node.js
    if test -d $NODENV_ROOT
        source (nodenv init -|psub)
    end

    # Crystal
    if test -d $CRENV_ROOT
        source (crenv init -|psub)
    end
end
